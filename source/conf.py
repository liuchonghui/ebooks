# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
project = 'ebooks'
copyright = '2024, ebooks'
author = 'ebooks'
release = '1.0'

import sphinx_rtd_theme
import recommonmark

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx_rtd_theme",
    "recommonmark",
    "sphinx_markdown_tables",
    "sphinx.ext.autosectionlabel"
]

source_suffix = ['.rst', '.md', '.txt']

templates_path = ['_templates']

exclude_patterns = []

master_doc = 'index'

language = 'zh_CN'

pygments_style = None

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

htmlhelp_basename = 'ebooksdoc'